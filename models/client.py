from sqlalchemy import Column, ForeignKey, Integer, String

from database import BaseModel

class Client(BaseModel):
    __tablename__ = 'client'

    id = Column(Integer, primary_key=True)
    name_client = Column(String)
    city_id = Column(Integer, ForeignKey('city.id'))
    email = Column(String)
